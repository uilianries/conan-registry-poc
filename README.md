# conan-registry-poc

Validate Gilab with Conan registry feature

## Steps

- Build Conan package fmt/10.1.1@acme/stable (copy from CCI)
- Upload Conan package fmt/10.1.1@acme/stable to Gilab Conan Registry (https://gitlab.com/api/v4/projects/53015283/packages/conan)
- Build Conan package spdlog/1.12.0@acme/stable (copy from CCI) consuming fmt/10.1.1@acme/stable from Gilab
- Upload Conan package spdlog/1.12.0@acme/stable to Gilab Conan Registry
- Search fmt/10.1.1@acme/stable spdlog/1.12.0@acme/stable in Gilab Conan Registry with no login
- Search fmt/10.1.1@acme/stable spdlog/1.12.0@acme/stable in Gilab Conan Registry with revisions

## License

![MIT](LICENSE)
